var ALIAS_MAX_LENGTH        = 20;
var ARTICLE_MAX_LENGTH      = 100;
var ROOM_OFFSET             = script.env.ROOM_OFFSET;
var MAX_ROOMS               = script.env.MAX_ROOMS;
var MAX_USERS_PER_ROOM      = script.env.MAX_USERS_PER_ROOM;

var token                   = script.env.TOKEN.split(";")
var articleid               = token[0]
var articlename             = token[1]
var alias                   = token[2]

if( connection.get("authenticated") == "yes" ){
    exit(0, "ALREADY_AUTHENTICATED")
}

if( articleid.length > ARTICLE_MAX_LENGTH || articleid.length < 2 ){
    exit(0, "BAD_ARTICLE_FORMAT");
}

if(alias.length > ALIAS_MAX_LENGTH || alias.length < 2 ){
    exit(0, "BAD_ALIAS_FORMAT");
}

connection.set("alias", alias );
connection.set("article", articleid );
connection.set("authenticated", "yes");

// Convert the connection id to a hex-string
connid = connection.id.toString(16);

// Variables
var room                = null;

for (var id = ROOM_OFFSET; id < MAX_ROOMS; id++) {
  room = domain.getChannel(id);
  if( room.get("active") == "yes" && room.get("articleid") == articleid ) {
      
      if (room.incr("count", MAX_USERS_PER_ROOM) == false) {
          exit(0, "ROOM_IS_FULL");
      }
      
      room.push("connections", [connid, alias].join(","));

      message = "notif:user-join " + [connid, alias].join(",");
      room.emit(message);
      
      exit(id)
  }
}

for (var id = ROOM_OFFSET; id < MAX_ROOMS; id++) {
  room = domain.getChannel(id);
  
  if( room.get("active") != "yes" ) {

      room.set("active", "yes")
      room.set("articleid", articleid )
      
      room.push("connections", [connid, alias].join(","));
      
      room.incr("count", MAX_USERS_PER_ROOM)
      
      message = "notif:user-join " + [connid, alias].join(",");
      room.emit(message);
      
      exit(id)
  }
}

exit(0, "NO_FREE_CHANNELS");

