// what we need, articlename

LOBBY_CHANNEL = 1
ROOM_OFFSET = 2
MAX_ROOMS = 100
MONITOR_CHANNEL = MAX_ROOMS + 1
MAX_USERS_PER_ROOM = 50
PASSWORD = "hello"


SCRIPT_ENV = {
  
  LOBBY_CHANNEL:      LOBBY_CHANNEL,
  ROOM_OFFSET:        ROOM_OFFSET,
  MAX_ROOMS:          MAX_ROOMS,
  MAX_USERS_PER_ROOM: MAX_USERS_PER_ROOM
}

open

    channel = LOBBY_CHANNEL
        
        mode = "e"
            
            run("./onhandshake.js", SCRIPT_ENV)
            
            when = $CODE
                redirect($CODE)
            end
            
            deny($MESSAGE)
            
        end
        
        deny("CHANNEL_MUST_BE_OPENED_IN_E_MODE")
        
    end
    
    channel = MONITOR_CHANNEL
        
        mode = "r"
            token = PASSWORD
                allow()
            end
            deny("YOU_NEED_THE_RIGHT_PASSWORD")
        end
        
        deny("CHANNEL_MUST_BE_OPENED_IN_R_MODE") 
    
    end
    
end

emit
    
    for (var ROOM = 0; ROOM < MAX_ROOMS; ROOM++) {
    channel = (ROOM + ROOM_OFFSET)
        token = "get_user_list"
            run("./api_get_user_list.js", SCRIPT_ENV)
        end
    end
    }
    
end

close
    for (var ROOM = 0; ROOM < MAX_ROOMS; ROOM++) {
    channel = (ROOM + ROOM_OFFSET)
        run("./onleaveroom.js", SCRIPT_ENV)
    end
    }
end